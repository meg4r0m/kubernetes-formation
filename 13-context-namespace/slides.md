%title: Kubernetes
%author: meg4r0m


# Kubernetes : Namespace et Contextes


## Les Namespaces


<br>
* namespace : un espace cloisonné (potentiellement gestion de droits...)


* namespace : permettre de lancer plusieurs pods identiques sur un même cluster


* ordonner et sécuriser ses déploiements


* exemple : le namespace kube-system


```
kubectl get pods

kubectl get pods -n kube-system
```

----------------------------------------------------------------------------------


## Les Contextes


<br>
* contexte : qui sommes nous, où sommes nous ? (conf par défaut du namespace par ex)


<br>
* la config view : paramètre des kubeconfig

```
kubectl config view
```

<br>
* lister les contextes

```
kubectl config get-contexts
kubectl config current-context
```

<br>
* lister les namespaces

```
kubectl get namespace
```


---------------------------------------------------------------------------


## Création namespaces et contextes


<br>
* création d'un namespace

```
kubectl create namespace meg4r0m
kubectl create deploy monnginx --image nginx -n meg4r0m
```

<br>
* création d'un contexte

```
kubectl config set-context meg4r0m --namespace meg4r0m --user kubernetes-admin --cluster kubernetes
```

<br>
* switch de contexte

```
kubectl config use-context meg4r0m
```

* suppression : utiliser "delete"


