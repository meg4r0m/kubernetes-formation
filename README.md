# Kubernetes

## Tutoriels en français sur Kubernetes

- 0. Sommaire
- 1. Introduction
- 2. Notions et Définitions
- 3. Minikube
- 4. Cluster : prérequis
- 5. Cluster : installation
- 6. Autocomplete et alias
- 7. CLI: Premier pod
- 8. CLI : Premier service et exposition de ports
- 9. CLI : Premier scaling
- 10. Debug et états des ressources
- 11. Export des yamls
- 12. Lister les ressources
- 13. Namespaces et Contextes
- 14. Labels et Annotations
- 15. Pods : configuration
- 16. Healthchecks : liveness et readiness
- 17. Ressources : limits et requests
- 19. Volumes : principes, hostpath...
- 20. Volumes : PVC et PV
- 21. Volumes : PVC et nfs
- 22. Volumes : exemple BDD et multipods
- 23. Replicaset : templates et appariement de pods
- 24. Replicaset : hpa et autoscalling
- 25. Configmaps et secrets
- 26. Configmaps : utilisation par volume
- 27. Deployment : principe et recreate
- 28. Deployment : rolling update et stratégies
- 29. Deployment : rolling update et rollout
- 30. Statefulset : principes
- 31. Statefulset et headless
- 32. Statefulset : exemple cassandra


Open slides with https://github.com/visit1985/mdp