%title: Kubernetes
%author: meg4r0m

# RBAC : Service Account et Rôles


<br>
3 ressources importantes et combinées :

* cluster role : un rôle est un profil permettant des accès/actions/ressources

* service account : user applicatif

* cluster role binding : association d'un service account à un rôle

------------------------------------------------------------------------


# Cluster Role



```
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: cr1
rules:
- apiGroups: [""] # "" indicates the core API group
  resources: ["configmaps"]
  verbs: ["get", "list", "watch", "patch"]
- apiGroups: [""] # "" indicates the core API group
  resources: ["pods"]
  verbs: ["describe"]
```


--------------------------------------------------------------------------


# Service account

```
apiVersion: v1
kind: ServiceAccount
metadata:
 name: meg4r0m
```

# Cluster Role Binding

```
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: meg4r0m
subjects:
- kind: ServiceAccount
  name: meg4r0m
  namespace: default
roleRef:
  kind: ClusterRole
  name: mysa1
  apiGroup: rbac.authorization.k8s.io
```

--------------------------------------------------------------------------


# Récupération du token


<br>
* récupération du token

```
kubectl get sa meg4r0m -o yaml
```

<br>
* visualisation du secrets

```
kubectl get secrets meg4r0m-token-g2gqt
```

---------------------------------------------------------------------------

# Pods de test


```
apiVersion: v1
kind: Pod
metadata:
  name: pod-default
  namespace: default
spec:
  #serviceAccountName: meg4r0m
  containers:
  - image: alpine:3.9
    name: alpine
    command:
    - sleep
    - "10000"
    volumeMounts:
    - mountPath: /var/run/secrets/kubernetes.io/serviceaccount
      name: meg4r0m-token-g2gqt
  volumes:
  - name: meg4r0m-token-g2gqt
    secret:
      secretName: meg4r0m-token-g2gqt
```

---------------------------------------------------------------------------

# Test


```
kubectl exec -ti pod-default -- sh
apk add --update curl
TOKEN=$(cat /run/secrets/kubernetes.io/serviceaccount/token)
curl -H "Authorization: Bearer $TOKEN" https://kubernetes/api/v1/ --insecure
curl -H "Authorization: Bearer $TOKEN" https://kubernetes/api/v1/services --insecure
curl -H "Authorization: Bearer $TOKEN" https://kubernetes/api/v1/pods --insecure
```

